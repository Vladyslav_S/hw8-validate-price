"use strict";

/* 1. Опишите своими словами, как Вы понимаете, что такое обработчик событий.
Это инструменты для сбора информации, какаие операции пользователь выполняет на странице и как мы на это реагируем */

const input = document.getElementById("input");
const form = document.getElementById("form");

input.addEventListener("focus", function () {
  input.style.border = "3px solid green";
});

input.addEventListener("blur", function () {
  // To remove existed elements
  if (document.getElementById("price") !== null) {
    price.remove();
  }
  if (document.getElementById("x") !== null) {
    x.remove();
  }
  if (document.getElementById("spanError") !== null) {
    spanError.remove();
  }

  if (input.value < 0) {
    input.style.border = "3px solid red";

    const spanError = document.createElement("span");
    spanError.id = "spanError";
    spanError.textContent = "Please enter correct price";

    form.after(spanError);
  } else {
    input.style.border = "";
    input.style.color = "green";

    const price = document.createElement("span");
    price.id = "price";
    price.textContent = `Текущая цена: ${input.value}  `;

    const x = document.createElement("span");
    x.innerHTML = "&#10005";
    x.id = "x";
    x.style.color = "white";
    x.style.backgroundColor = "red";
    x.style.border = "1px solid black";
    x.style.cursor = "pointer";

    x.addEventListener("click", function () {
      price.remove();
      x.remove();
    });

    const fragment = document.createDocumentFragment();
    fragment.append(price);
    fragment.append(x);

    form.before(fragment);
  }
});

setInputFilter(input, function (value) {
  return /^\d*\-?\d*$/.test(value); // Allow digits and '-' only, using a RegExp
});

// Restricts input for the given textbox to the given inputFilter function.
function setInputFilter(textbox, inputFilter) {
  [
    "input",
    "keydown",
    "keyup",
    "mousedown",
    "mouseup",
    "select",
    "contextmenu",
    "drop",
  ].forEach(function (event) {
    textbox.addEventListener(event, function () {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}
